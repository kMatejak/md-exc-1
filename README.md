# Zadanie 1  
  
**[Link do rozwiązania tego zadania na GitLab](https://gitlab.com/kMatejak/md-exc-1)**  
  
## I. Wybrany plik graficzny  
  
![Image of Octocat](https://image.flaticon.com/icons/png/512/35/35847.png)  
  
## II. Lista 3 książek  
  
1. The Tragedy of Hamlet, Prince of Denmark, by William Shakespeare  
2. Meditations, by Marcus Aurelius  
3. Learn Python 3 the Hard Way, by Zed A. Shaw  
  
## III. Lista 5 miast  
  
- Maszewo  
- Nysa  
- Siedlce  
- Warszawa  
- Wrocław  
  
## IV. Dwa odnośniki  
  
1. [Mastering Markdown](https://guides.github.com/features/mastering-markdown/)  
2. [Python Learning Paths](https://realpython.com/learning-paths/)  
  
## V. Zdanie stylizowane  
  
Lorem ipsum **dolor sit amet**, consectetur adipisicing elit, _sed do eiusmod
tempor_ incididunt ut labore et dolore ~~paulo multum~~ magna aliqua.  
  
## VI. Wzór matematyczny  
<!-- About GitLab math rendering https://docs.gitlab.com/ee/user/markdown.html#math -->  
$`{\frac d {dx}}( 2x^2 + x )^3 = 3( 2x^2 + x )^2 ( 4x + 1 )`$  
  
## VII. Dwie linie tekstu z nagłówkami 2 i 3 poziomu  
  
To był pierwszy z dwóch nagłówków (nagłówek 2-go poziomu).  
  
### A to jest drugi z dwóch nagłówków (nagłówek 3-go poziomu)  
  
Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
proident, sunt in culpa qui officia deserunt mollit anim id est laborum.  
